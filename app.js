const http = require('http');
const express = require('express');
const bodyparser = require("body-parser");
const app = express()
const port = 3003
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'))
app.use(bodyparser.urlencoded({extended:true}));


let datos =[{
    matricula:"2020020309",
    nombre:"Acosta Ortega Jesus Humberto",
    sexo:'M',
    materias:["Ingles","Tecnologia I","Base de Datos"]
},{
    matricula:"2020020310",
    nombre:"Amolgar Vazquez Yarlen de Jesus",
    sexo:'F',
    materias:["Ingles","Tecnologia I","Base de Datos"]
},{
    matricula:"20200200240",
    nombre:"Saul Eduardo Martinez Valdes",
    sexo:'M',
    materias:["Ingles","Tecnologia I","Base de Datos"]
},{
    matricula:"2020020397",
    nombre:"Carlos Omar Osuna Pineda",
    sexo:'M',
    materias:["Ingles","Tecnologia I","Base de Datos"]
},{
    matricula:"2020020206",
    nombre:"Juan Carlos Moreno Lopez",
    sexo:'M',
    materias:["Ingles","Tecnologia I","Base de Datos"]
},{
    matricula:"2020020324",
    nombre:"Victor Uriel Juarez Lugo",
    sexo:'M',
    materias:["Ingles","Tecnologia I","Base de Datos"]
}]


app.get('/',(req,res)=>{
    //res.send('Iniciamos con express')
    res.render('index',{titulo:"Listado de Alumnos",listado:datos})
})


app.get("/tablas",(req,res)=>{

const valores = {
    tabla:req.query.tabla

    }
res.render('tablas',valores);

})

app.post("/tablas",(req,res)=>{
    const valores={
        tabla:req.body.tabla
    }

res.render('tablas',valores);
})

app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.get('/cotizacion', (req, res) => {
    const val ={
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
    }
    res.render('cotizacion',val);
});

app.post('/cotizacion', (req, res) => {
    const val ={
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('cotizacion',val);
});

app.use((req,res,next)=>{

res.status(404).sendFile(__dirname + '/public/error.html')

})


app.use(express.static(__dirname + '/public'))

app.listen(port,()=>{
    console.log('Inciado el puerto 3003')
})
